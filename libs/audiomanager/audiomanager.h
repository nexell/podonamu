#ifndef AUDIOMANAGER_H
#define AUDIOMANAGER_H


#include <QObject>

#include <QAudioDeviceInfo>
#include <QAudioOutput>
#include <QAudioInput>
#ifdef Q_OS_LINUX
#include <alsa/asoundlib.h>
#endif



class AudioManager : public QObject
{
    Q_OBJECT
public:
    explicit AudioManager(QObject *parent = 0);
    ~AudioManager();
    Q_INVOKABLE bool isDefaultOutputAudioDevice();
    Q_INVOKABLE int getAudioVolume();
    Q_INVOKABLE void setAudioVolume(long value);

    Q_INVOKABLE bool isMuted() const;
    Q_INVOKABLE void setMuted(bool value);
signals:

public slots:


private:
#ifdef Q_OS_LINUX
    snd_mixer_t *m_alsaMixer;
    snd_mixer_selem_id_t *m_selemId;
    snd_mixer_elem_t *m_selem;
#endif
    long m_min, m_max;

};

#endif // AUDIOMANAGER_H
