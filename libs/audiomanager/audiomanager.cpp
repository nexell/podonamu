#include "audiomanager.h"
#include <QDebug>

AudioManager::AudioManager(QObject *parent) : QObject(parent)
  , m_min(0)
  , m_max(0)
{
#ifdef Q_OS_LINUX
    m_alsaMixer = Q_NULLPTR;
    m_selemId = Q_NULLPTR;
    m_selem = Q_NULLPTR;

    if (snd_mixer_open(&m_alsaMixer, 0) < 0) {
        qWarning() << "AudioManager::AudioManager Failed to open Alsa mixer";
        return;
    }
    snd_mixer_attach(m_alsaMixer, "default");
    snd_mixer_selem_register(m_alsaMixer, 0, 0);
    snd_mixer_load(m_alsaMixer);

    snd_mixer_selem_id_alloca(&m_selemId);
    snd_mixer_selem_id_set_index(m_selemId, 0);
    snd_mixer_selem_id_set_name(m_selemId, "Master");
    m_selem = snd_mixer_find_selem(m_alsaMixer,
                                   m_selemId);
    if (!m_selem) {
        qWarning() << "AudioManager::AudioManager Failed to find Master element";
        return;
    }
    snd_mixer_selem_get_playback_volume_range(m_selem,
                                              &m_min,
                                              &m_max);
    qDebug() << "AudioManager::AudioManager m_min : " << m_min << "m_max:" << m_max;
#endif


}

AudioManager::~AudioManager()
{
#ifdef Q_OS_LINUX
    if (m_alsaMixer)
        snd_mixer_close(m_alsaMixer);
    delete m_alsaMixer;
    delete m_selem;
    delete m_selemId;
#endif
}

bool AudioManager::isDefaultOutputAudioDevice()
{
    #ifdef Q_OS_LINUX
    return m_alsaMixer && m_selem;
    #endif
    return false;
}

int AudioManager::getAudioVolume()
{
    long vol = 0;
#ifdef Q_OS_LINUX
    if (m_selem) {
        snd_mixer_selem_get_playback_volume(m_selem, SND_MIXER_SCHN_FRONT_LEFT, &vol);
    }
    vol = qRound(vol*100.0/m_max);
    qDebug() << "AudioManager::AudioManager vol : " << vol;
#endif
    return vol;
}

void AudioManager::setAudioVolume(long value)
{
#ifdef Q_OS_LINUX
    // Volume must be in range
    long convertValue = m_max * value /100;
    if (convertValue < m_min) {
        qWarning() << "Volume" << value << "too low, assume" << m_min;
        value = m_min;
    }
    if (convertValue > m_max) {
        qWarning() << "Volume" << value << "too high, assume" << m_max;
        value = m_max;
    }
    if (m_selem) {
        snd_mixer_selem_set_playback_volume(m_selem, SND_MIXER_SCHN_FRONT_LEFT, convertValue);
        snd_mixer_selem_set_playback_volume(m_selem, SND_MIXER_SCHN_FRONT_RIGHT, convertValue);
    }
#endif
}


bool AudioManager::isMuted() const
{
    int mute = 0;
#ifdef Q_OS_LINUX
    snd_mixer_selem_get_playback_switch(m_selem, SND_MIXER_SCHN_FRONT_LEFT, &mute);
#endif
    return !mute;
}

void AudioManager::setMuted(bool value)
{
#ifdef Q_OS_LINUX
    snd_mixer_selem_set_playback_switch_all(m_selem, !value);
#endif
}

