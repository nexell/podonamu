TEMPLATE = lib
DESTDIR = ..
TARGET = pdaudiomanager
CONFIG += static

QT += multimedia

MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj

INCLUDEPATH += ../common

HEADERS += \
    audiomanager.h

SOURCES += \
    audiomanager.cpp

LIBS += -lasound
