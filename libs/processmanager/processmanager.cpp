#include "processmanager.h"

#include "pdglobal.h"

ProcessManager::ProcessManager(QObject *parent) : QObject(parent)
{
    m_packageManager =0;
    m_applicationManager = 0;
}

ProcessManager::~ProcessManager()
{
    for(int i= 0 ; i < m_processList.count(); i++)
    {
        QProcess* process = m_processList.value(i);
        if(process)
        {
            process->deleteLater();
        }
    }
    m_processList.clear();
    m_processMap.clear();
}

PackageManager *ProcessManager::packageManager() const
{
    return m_packageManager;
}

void ProcessManager::setPackageManager(PackageManager* packageManager)
{
    m_packageManager = packageManager;
}

void ProcessManager::setApplicationManager(ApplicationManager* applicationManager)
{
    m_applicationManager = applicationManager;
}

void ProcessManager::executeApplication(const QString& name)
{
    qDebug() << "ProcessManager::executeApplication name :" << name;

    QString appExecPath = m_packageManager->execPath(name);
    QString appType = m_packageManager->appType(name);
    if(appExecPath.isEmpty())
    {
        emit doNotExistExecPath(name);
    }
    qDebug() << "ProcessManager::executeApplication appExecPath :" << appExecPath;
    if(m_processMap.contains(name))
    {
        emit alreadyExecuted(name);
    }
    else
    {
        QProcess *process = new QProcess();
        connect(process, SIGNAL(finished(int)), this, SLOT(finishedProcess()));
        QStringList arguments;

        if(appType=="Application")
        {
            process->start(appExecPath);

        }
        else if(appType == "WebApp")
        {
            QString defaultWebAppPath = PD_DEFAULT_WEBENGINE_APP;
            QStringList arguments;
            arguments << "-url" << "file://" + appExecPath;
            process->start(defaultWebAppPath,arguments);
        }
        m_processMap.insert(name, process);
        m_processList.append(process);
        m_applicationManager->addExcuteApplication(name);
    }
}

void ProcessManager::finishedProcess()
{
    QProcess* process = (QProcess *)sender();
    QString name = m_processMap.key(process);
    m_processMap.remove(name);
    m_processList.removeAll(process);
    m_applicationManager->FinishedApplication(name);
    process->deleteLater();
    qDebug() << "ProcessManager::processFinished name : " << name;
    emit finishedApplication(name);


}
