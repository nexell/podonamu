TEMPLATE = lib
DESTDIR = ..
TARGET = pdprocessmanager
CONFIG += static
QT +=

MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj

INCLUDEPATH += ../common

HEADERS += \
    processmanager.h

SOURCES += \
    processmanager.cpp

INCLUDEPATH += ..
LIBS += -L.. -lpdpackagemanager -lpdapplicationmanager
