#ifndef PROCESSMANAGER_H
#define PROCESSMANAGER_H

#include <QObject>
#include <QDebug>
#include <QProcess>
#include "packagemanager/packagemanager.h"
#include "applicationmanager/applicationmanager.h"


typedef QMap<QString, QProcess*>  ProcessMap;
typedef QList<QProcess*>  ProcessList;

class ProcessManager : public QObject
{
    Q_OBJECT
public:
    explicit ProcessManager(QObject *parent = 0);
    ~ProcessManager();
    PackageManager *packageManager() const;
    void setPackageManager(PackageManager *packageManager);
    void setApplicationManager(ApplicationManager* applicationManager);

public slots:
    void executeApplication(const QString& name);

private slots:
    void finishedProcess();

private:
    PackageManager* m_packageManager;
    ApplicationManager* m_applicationManager;
    ProcessList m_processList;
    ProcessMap m_processMap;

signals:
    Q_INVOKABLE void doNotExistExecPath(const QString& name);
    Q_INVOKABLE void alreadyExecuted(const QString& name);
    Q_INVOKABLE void finishedApplication(const QString& name);
};

#endif // PROCESSMANAGER_H
