#ifndef PDGLOBAL_H
#define PDGLOBAL_H

#if QT_VERSION < 0x050000
#include <QDesktopServices>
#else
#include <QStandardPaths>
#endif

namespace PD {

#define PD_LAUNCHER_PREFIX      "pdlauncher"

#ifdef Q_OS_LINUX

#define PD_DEFAULT_PATH     "/podo"
#define PD_DEFAULT_APPS_PATH     "/podo/apps"
#define PD_DEFAULT_PACKAGE_PATH     "/podo/package"
#define PD_DEFAULT_PAGE_PATH     "/podo/page"

#else

#if QT_VERSION < 0x050000
#define PD_DEFAULT_PATH     QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/podo"
#define PD_DEFAULT_APPS_PATH     QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/podo/apps"
#define PD_DEFAULT_PACKAGE_PATH  QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/podo/package"
#define PD_DEFAULT_PAGE_PATH     QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "/podo/page"
#else
#define PD_DEFAULT_PATH     QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + "/podo"
#define PD_DEFAULT_APPS_PATH     QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + "/podo/apps"
#define PD_DEFAULT_PACKAGE_PATH  QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + "/podo/package"
#define PD_DEFAULT_PAGE_PATH     QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + "/podo/page"
#endif // QT_VERSION < 0x050000

#endif // Q_OS_LINUX
#define PD_DEFAULT_WEBENGINE_APP    QString(PD_DEFAULT_APPS_PATH) + "/pdwebengineview/pdwebengineview"

}

#endif // PDGLOBAL_H
