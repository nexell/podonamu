TEMPLATE = lib
DESTDIR = ..
TARGET = pdnetworkmanager
CONFIG += static
QT += network
MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj

INCLUDEPATH += ../common
SOURCES +=\
        networkmanager.cpp

HEADERS  += networkmanager.h
