#include "networkmanager.h"

#include <QDebug>
#include <QNetworkInterface>
#include <QNetworkAccessManager>
#include <QFile>

#ifdef Q_OS_LINUX
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

NetworkManager::NetworkManager(QObject *parent) : QObject(parent)
{

}

NetworkManager::~NetworkManager()
{

}

void NetworkManager::setDhcpNetwork(bool isDhcp)
{
    qDebug() << "NetworkManager::setDhcpNetwork isDhcp" << isDhcp;
#ifdef Q_OS_LINUX
    QString filename = "/etc/network/interfaces";
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        qWarning() << "NetworkManager::setDhcpNetwork"
                   << " failed to open file:" << filename
                   << " error - " << file.error();
        return;
    }

    file.write("auto lo\niface lo inet loopback\n\n");
    file.write("auto eth0\n");

    if (isDhcp)
    {
        file.write("iface eth0 inet dhcp\n");
    }
    file.close();
#endif
}

void NetworkManager::loadNetworkInfo()
{
    QList<QNetworkInterface> list = QNetworkInterface::allInterfaces();
    foreach(QNetworkInterface ifce, list)
    {
        unsigned int flags = ifce.flags();
        bool isLoopback = (bool)(flags & QNetworkInterface::IsLoopBack);
        bool isP2P = (bool)(flags & QNetworkInterface::IsPointToPoint);
        bool isRunning = (bool)(flags & QNetworkInterface::IsRunning);

        if (isLoopback || isP2P) continue;
        if (!isRunning) continue;
        m_interfaceNameMap[ifce.humanReadableName()] = QVariant(ifce.name());
        m_interfaceList.append(QVariant(ifce.humanReadableName()));
    }
    qDebug() << m_interfaceList;
#ifdef Q_OS_LINUX
    // dns
    QString dnsFilename = "/etc/resolv.conf";
    QFile dnsFile(dnsFilename);
    if (!dnsFile.open(QIODevice::WriteOnly))
    {
        qWarning() << "SetupDialog::loadNetworkInfo"
                   << " failed to open file:" << dnsFilename;
    }
    else
    {
        while (!dnsFile.atEnd())
        {
            QString line = dnsFile.readLine().simplified();
            QStringList lineList = line.split(" ");
            if (lineList.size() >= 2 && lineList[0] == "nameserver")
            {
                m_nameserverList.append(QVariant(lineList[1]));
            }

        }
    }
#endif
}

void NetworkManager::setIpInformationNetwork(QString ip, QString netmask, QString gateway)
{
    qDebug() << "NetworkManager::setIpInformationNetwork ip:" << ip;
    qDebug() << "NetworkManager::setIpInformationNetwork netmask:" << netmask;
    qDebug() << "NetworkManager::setIpInformationNetwork gateway:" << gateway;
#ifdef Q_OS_LINUX
    qDebug() << "NetworkManager::ipInformationNetwork ip:" << ip;
    qDebug() << "NetworkManager::ipInformationNetwork netmask:" << netmask;
    qDebug() << "NetworkManager::ipInformationNetwork gateway:" << gateway;

    QString filename = "/etc/network/interfaces";
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly))
    {
        qWarning() << "NetworkManager::setIpInformationNetwork"
                   << " failed to open file:" << filename;
        return;
    }

    file.write("iface eth0 inet static\n");

    QString line;

    line.sprintf("address %s\n", ip.toUtf8().data());
    file.write(qPrintable(line));

    line.sprintf("netmask %s\n", netmask.toUtf8().data());
    file.write(qPrintable(line));

    line.sprintf("gateway %s\n", gateway.toUtf8().data());
    file.write(qPrintable(line));

    file.close();
#endif
}

bool NetworkManager::isDhcpNetwork(const QString &interface)
{
#ifdef Q_OS_LINUX
    QFile file("/etc/network/interfaces");
    if (!file.open(QIODevice::ReadOnly))
    {
        qWarning() << "NetworkManager::isDhcpNetwork"
                   << " failed to open /etc/network/interfaces "
                   << "error - " << file.error();
        return false;
    }
    while (!file.atEnd())
    {
        QString line = file.readLine().simplified();
        QStringList lineList = line.split(" ");
        if (lineList.size() != 4) continue;
        if (lineList[1] == interface && lineList[3] == "dhcp")
            return true;
    }
#else
#endif
    return false;
}
QString NetworkManager::defaultGateway()
{
    QString gateway;
#ifdef Q_OS_LINUX
    QFile netRouteFile("/proc/net/route");
    QTextStream ts(&netRouteFile);
    if (!netRouteFile.open(QIODevice::ReadOnly))
    {
        qWarning() << "SetupDialog::defaultGateway"
                   << " failed to open /proc/net/route";
        return gateway;
    }
    QString line;
    do
    {
        line = ts.readLine();
        //qDebug() << "SetupDialog::defaultGateway line:" << line;
        if (line.simplified().section(' ', 1, 1) == QString("00000000"))
        {
            struct sockaddr_in name;
            bool ok=true;
            name.sin_addr.s_addr = line.simplified().section( ' ', 2, 2 ).toUInt(&ok,16);
            gateway = inet_ntoa(name.sin_addr);
            return gateway;
        }
    }
    while (!line.isNull());
#else
#endif

    return gateway;
}
QVariantMap NetworkManager::changedNetworkInterface(const QString& interface)
{
    QVariantMap networkInfo;

    if (!m_interfaceNameMap.contains(interface))
    {
        qWarning() << "SetupDialog::changedNetworkInterface"
                   << " does not contain interface:" << interface;
        return networkInfo;
    }
    QString name = m_interfaceNameMap[interface].toString();
    qDebug() << "SetupDialog::changedNetworkInterface"
            << " interface:" << interface << " name:" << name;

    QNetworkInterface ifce = QNetworkInterface::interfaceFromName(name);

    QList<QNetworkAddressEntry> entries = ifce.addressEntries();

    foreach(QNetworkAddressEntry entry, entries)
    {
        QStringList ipList = entry.ip().toString().split(".");
        if (ipList.size() != 4) continue; // skip ipv6
        networkInfo.insert("ip" , QVariant(entry.ip().toString()));
        networkInfo.insert("netmask" , QVariant(entry.netmask().toString()));
    }
    networkInfo.insert("isDhcp" , QVariant(isDhcpNetwork(name)));
    networkInfo.insert("defaultGateway" , QVariant(defaultGateway()));
    return networkInfo;

}

QVariantList NetworkManager::interfaceList() const
{
    return m_interfaceList;
}

QVariantList NetworkManager::nameserverList() const
{
    return m_nameserverList;
}

QVariantMap NetworkManager::interfaceNameMap() const
{
    return m_interfaceNameMap;
}


