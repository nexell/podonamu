#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QVariantList>
#include <QVariantMap>
#include <QNetworkReply>


class NetworkManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariantList interfaceList READ interfaceList NOTIFY interfaceListChanged)
    Q_PROPERTY(QVariantList nameserverList READ nameserverList NOTIFY nameserverListChanged)
    Q_PROPERTY(QVariantMap interfaceNameMap READ interfaceNameMap NOTIFY interfaceNameMapChanged)

private:
    QVariantList m_interfaceList;
    QVariantList m_nameserverList;
    QVariantMap m_interfaceNameMap;

public:
    explicit NetworkManager(QObject *parent = 0);
    ~NetworkManager();

    Q_INVOKABLE void setDhcpNetwork(bool isDhcp);
    Q_INVOKABLE void loadNetworkInfo();
    Q_INVOKABLE void setIpInformationNetwork(QString ip, QString netmask, QString gateway);
    Q_INVOKABLE bool isDhcpNetwork(const QString& humanReadableName);
    Q_INVOKABLE QVariantMap changedNetworkInterface(const QString& interface);
    QVariantList interfaceList() const;
    QVariantList nameserverList() const;

    QVariantMap interfaceNameMap() const;

    QString defaultGateway();

signals:
    void interfaceListChanged(QVariantList interfaceList);
    void nameserverListChanged(QVariantList nameserverList);
    void interfaceNameMapChanged(QVariantMap interfaceNameMap);
};

#endif // NETWORKMANAGER_H
