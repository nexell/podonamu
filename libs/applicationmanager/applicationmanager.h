#ifndef APPLICATIONMANAGER_H
#define APPLICATIONMANAGER_H

#include <QObject>
#include <QList>

class ApplicationManager : public QObject
{
    Q_OBJECT
public:
    explicit ApplicationManager(QObject *parent = 0);

    bool addExcuteApplication(const QString& name);
    bool FinishedApplication(const QString& name);

public slots:

signals:

private:
    QList<QString> m_excuteApplicationList;

};

#endif // APPLICATIONMANAGER_H
