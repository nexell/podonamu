TEMPLATE = lib
DESTDIR = ..
TARGET = pdapplicationmanager
CONFIG += static
QT +=

MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj

INCLUDEPATH += ../common

HEADERS += \
    applicationmanager.h

SOURCES += \
    applicationmanager.cpp
