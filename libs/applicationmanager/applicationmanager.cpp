#include "applicationmanager.h"

ApplicationManager::ApplicationManager(QObject *parent) : QObject(parent)
{

}

bool ApplicationManager::addExcuteApplication(const QString &name)
{
    if(m_excuteApplicationList.contains(name))
        return false;
    m_excuteApplicationList.append(name);
    return true;
}

bool ApplicationManager::FinishedApplication(const QString &name)
{
    m_excuteApplicationList.removeAll(name);
    return true;
}
