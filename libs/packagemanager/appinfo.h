#ifndef APPINFO_H
#define APPINFO_H

#include <QObject>
#include <QList>
#include <QMap>

class AppInfo : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type WRITE setType)
    Q_PROPERTY(QString version READ version WRITE setVersion)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString genericName READ genericName WRITE setGenericName)
    Q_PROPERTY(QString comment READ comment WRITE setComment)
    Q_PROPERTY(QString icon READ icon WRITE setIcon)
    Q_PROPERTY(QString tryExec READ tryExec WRITE setTryExec)
    Q_PROPERTY(QString exec READ exec WRITE setExec)
    Q_PROPERTY(QString path READ path WRITE setPath)
    Q_PROPERTY(QString url READ url WRITE setUrl)
    Q_PROPERTY(QString keys READ keys WRITE setKeys)
    Q_PROPERTY(bool installed READ installed WRITE setInstalled)
    Q_PROPERTY(QString packagePath READ packagePath WRITE setPackagePath)

private:
    // http://standards.freedesktop.org/desktop-entry-spec/latest/ar01s05.html
    QString m_type; // Application(type 1), Link(type 2), Directory(type 3)
    QString m_version;
    QString m_name; // Specific name of the application, for example "Mozilla".
    QString m_genericName; // Generic name of the application, for example "Web Browser".
    QString m_comment;
    QString m_icon;
    QString m_tryExec; // Path to an executable file on disk used to determine if the program is actually installed
    QString m_exec; // Program to execute, possibly with arguments.
    QString m_path; // If entry is of type Application, the working directory to run the program in
    QString m_url; // If entry is Link type, the URL to access.
    QString m_keys; // Application english name

    QString m_packagePath;

    bool m_installed;

public:
    explicit AppInfo();

    QString type() const;
    void setType(const QString& type);

    QString version() const;
    void setVersion(const QString& version);

    QString name() const;
    void setName(const QString& name);

    QString genericName() const;
    void setGenericName(const QString& genericName);

    QString comment() const;
    void setComment(const QString& comment);

    QString icon() const;
    void setIcon(const QString& icon);

    QString tryExec() const;
    void setTryExec(const QString& tryExec);

    QString exec() const;
    void setExec(const QString& exec);

    QString path() const;
    void setPath(const QString& path);

    QString url() const;
    void setUrl(const QString& url);

    QString keys() const;
    void setKeys(const QString& keys);

    bool installed() const;
    void setInstalled(bool installed);

    QString packagePath() const;
    void setPackagePath(const QString& packagePath);

};

typedef QList<AppInfo*> AppInfoList;
typedef QMap<QString, AppInfo*> AppInfoMap;

#endif // APPINFO_H
