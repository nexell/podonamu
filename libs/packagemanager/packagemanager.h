#ifndef PACKAGEMANAGER_H
#define PACKAGEMANAGER_H

#include <QObject>
#include <QVariantList>
#include "appinfo.h"

class PackageManager : public QObject
{
    Q_OBJECT
private:
    AppInfoList m_appInfoList;
    AppInfoMap m_appInfoMap;
    AppInfoList m_uninstalledAppInfoList;
    AppInfoMap m_uninstalledAppInfoMap;

public:
    explicit PackageManager(QObject *parent = 0);
    ~PackageManager();

    AppInfoList appInfoList();
    Q_INVOKABLE QVariantList uninstalledAppInfoVariantList();
    Q_INVOKABLE QVariantList appInfoVariantList();
    QString execPath(const QString& name);
    QString appType(const QString &name);

    AppInfoMap appInfoMap();
    Q_INVOKABLE void reload();
    Q_INVOKABLE bool install(const QString& key);
    Q_INVOKABLE bool remove(const QString& pkg);
    Q_INVOKABLE QVariant appInfoVariant(const QString& key);
    Q_INVOKABLE QVariant packageAppInfoVariant(const QString& key);

private:
    void load();
    void loadUninstalled();
    AppInfo* loadAppInfo(const QString &key, const QString& desktopFile);
    void clearAll();
    bool removeDir(const QString & dirName);
};

#endif // PACKAGEMANAGER_H
