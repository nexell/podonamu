#include "packagemanager.h"
#include "pdglobal.h"
#include <QDir>
#include <QSettings>
#include <QLocale>
#include <QStringList>
#include <QDebug>
#include <QVariant>
#include <QTextCodec>


PackageManager::PackageManager(QObject *parent) :
    QObject(parent)
{
    load();
    loadUninstalled();
}


PackageManager::~PackageManager()
{
    qDeleteAll(m_appInfoList);
    m_appInfoList.clear();
    qDeleteAll(m_uninstalledAppInfoList);
    m_uninstalledAppInfoMap.clear();
}

void PackageManager::load()
{

    //qDebug() << "PackageManager::load language:" << language;

    QString appsPath = qgetenv("PODO_APPS_PATH");

    if (appsPath.isEmpty())
        appsPath = PD_DEFAULT_APPS_PATH;

    QDir dir(appsPath);

    if (!dir.exists(appsPath))
    {
        qWarning() << "PackageManager::load apps directory not valid path:" << appsPath;
        return;
    }

    QStringList dirList = dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);

    //qDebug() << "PackageManager::load packageCount:" << dirList.count();

    for (int i=0; i<dirList.size(); i++)
    {
        // find .desktop
        QString desktopFile = dir.absolutePath() + "/" + dirList.at(i) + "/" +
                dirList.at(i) + ".desktop";

        //qDebug() << "PackageManager::load found desktop file:" << desktopFile;

        if (!QFile::exists(desktopFile))
        {
            if (dirList.at(i) == "pdlauncher")
                continue;

            qWarning() << "PackageManager::load not found desktop file! path:" << desktopFile;

            continue;
        }

        AppInfo* appInfo  = loadAppInfo(dirList.at(i), desktopFile);

        if (!appInfo->type().isEmpty() &&
                !appInfo->name().isEmpty() &&
                !appInfo->keys().isEmpty())
        {

            QString path = dir.absolutePath() + "/" + dirList.at(i);
            appInfo->setPath(path);
#if 0 // debugging
            qDebug() << "PackageManager::load type:" << appInfo->type()
                     << " version:" << appInfo->version()
                     << " name:" << appInfo->name();

#endif
            appInfo->setInstalled(true);
            m_appInfoList.push_back(appInfo);
            m_appInfoMap[appInfo->keys()] = appInfo;

        }
        else
        {
            delete appInfo;
        }

    }
}

void PackageManager::loadUninstalled()
{
    //qDebug() << "PackageManager::load language:" << language;

    QString packagePath = qgetenv("PODO_PACKAGE_PATH");

    if (packagePath.isEmpty())
        packagePath = PD_DEFAULT_PACKAGE_PATH;

    QDir dir(packagePath);

    if (!dir.exists(packagePath))
    {
        qWarning() << "PackageManager::load apps directory not valid path:" << packagePath;
        return;
    }
    QString desktopsPath = dir.absolutePath() +"/" + "desktops";

    QStringList zipList = dir.entryList(QStringList("*.zip"),
                                        QDir::Files | QDir::NoDotAndDotDot,
                                        QDir::Name);

    //qDebug() << "PackageManager::load packageCount:" << dirList.count();

    for (int i=0; i<zipList.size(); i++)
    {

        // zip file info
        QString zipFilePath = dir.absolutePath() + "/" + zipList.at(i);
        QFileInfo fileInfo(zipFilePath);
        // find .desktop
        QString desktopFileName = fileInfo.completeBaseName()+ ".desktop";
        QString desktopFilePath = desktopsPath + "/" + desktopFileName;
        QFile::remove(desktopFilePath);
        if (!QFile::exists(desktopFilePath))
        {
#ifdef Q_OS_LINUX
            QString command = QString("unzip -o \"%1\" \"%2\" -d \"%3\"").arg(zipFilePath,desktopFileName,desktopsPath);
            system(qPrintable(command));
#endif
        }

        if (QFile::exists(desktopFilePath))
        {
            AppInfo* appInfo  = loadAppInfo(fileInfo.completeBaseName(),desktopFilePath);

            if (!appInfo->type().isEmpty() &&
                    !appInfo->name().isEmpty() &&
                    !appInfo->keys().isEmpty())
            {

#if 0 // debugging
                qDebug() << "PackageManager::load type:" << appInfo->type()
                         << " version:" << appInfo->version()
                         << " name:" << appInfo->name();

#endif
#ifdef Q_OS_LINUX
                QString command = QString("unzip -o \"%1\" \"%2\" -d \"%3\"").arg(zipFilePath,appInfo->icon(),desktopsPath);
                system(qPrintable(command));
#endif
                appInfo->setPath(desktopsPath);
                appInfo->setInstalled(m_appInfoMap.contains(appInfo->keys()));
                appInfo->setPackagePath(zipFilePath);

                m_uninstalledAppInfoList.push_back(appInfo);
                m_uninstalledAppInfoMap[appInfo->keys()] = appInfo;

            }
            else
            {
                delete appInfo;
            }
        }
    }
}

AppInfo *PackageManager::loadAppInfo(const QString &key, const QString &desktopFile)
{
    QString language = QLocale::system().bcp47Name();
    AppInfo* appInfo = new AppInfo();

    appInfo->setKeys(key);

    QSettings settings(desktopFile, QSettings::IniFormat);

    settings.setIniCodec("UTF-8");

    settings.beginGroup("Desktop Entry");
    QStringList keys = settings.allKeys();

    for (int keyIdx=0; keyIdx<keys.size(); keyIdx++)
    {
        QString key = keys.at(keyIdx);
        QString lowerKey = key.toLower();

        if (lowerKey == "type")
        {
            QString value = settings.value(key).toString();
            bool validType = false;
            if (value.toLower() == "application" ||
                    value.toLower() == "link" ||
                    value.toLower() == "webapp" ||
                    value.toLower() == "directory")
            {
                validType = true;
            }

            if (!validType)
            {
                qDebug() << "PackageManager::load invalid type:" << value
                         << " desktop file:" << desktopFile;
                continue;
            }
            appInfo->setType(value);
        }
        else if (lowerKey == "version")
        {
            appInfo->setVersion(settings.value(key).toString());
        }
        else if (lowerKey == "tryexec")
        {
            appInfo->setTryExec(settings.value(key).toString());
        }
        else if (lowerKey == "exec")
        {
            appInfo->setExec(settings.value(key).toString());
        }

        else if (lowerKey == QString("name[%1]").arg(language) ||
                 lowerKey == "name")
        {
            appInfo->setName(settings.value(key).toString());



        }
        else if (lowerKey == QString("comment[%1]").arg(language) ||
                 lowerKey == "comment")
        {
            appInfo->setComment(settings.value(key).toString());
        }
        else if (lowerKey == QString("icon[%1]").arg(language) ||
                 lowerKey == "icon")
        {
            appInfo->setIcon(settings.value(key).toString());
        }
        else
        {
            qWarning() << "PackageManager::load unknown key:" << key;
            continue;
        }
    }
    return appInfo;
}

void PackageManager::clearAll()
{
    qDeleteAll(m_appInfoList);
    m_appInfoList.clear();
    m_appInfoMap.clear();
    qDeleteAll(m_uninstalledAppInfoList);
    m_uninstalledAppInfoList.clear();
    m_uninstalledAppInfoMap.clear();
}

bool PackageManager::removeDir(const QString &dirName)
{
    bool result = true;
    QDir dir(dirName);

    if (dir.exists()) {
        Q_FOREACH(QFileInfo info, dir.entryInfoList(QDir::NoDotAndDotDot | QDir::System | QDir::Hidden  | QDir::AllDirs | QDir::Files, QDir::DirsFirst)) {
            if (info.isDir()) {
                result = removeDir(info.absoluteFilePath());
            }
            else {
                result = QFile::remove(info.absoluteFilePath());
            }

            if (!result) {
                return result;
            }
        }
        result = QDir().rmdir(dirName);
    }
    return result;
}

AppInfoList PackageManager::appInfoList()
{
    return m_appInfoList;
}

QVariantList PackageManager::uninstalledAppInfoVariantList()
{
    QVariantList retnList;

    foreach(AppInfo* appInfo, m_uninstalledAppInfoList)
    {
        //qDebug() << "PackageManager::appInfoVariantList"
        //          << " name:" << appInfo->name();
        retnList.append(qVariantFromValue((QObject*)appInfo));
    }

    return retnList;
}

QVariantList PackageManager::appInfoVariantList()
{
    QVariantList retnList;

    foreach(AppInfo* appInfo, m_appInfoList)
    {
        //qDebug() << "PackageManager::appInfoVariantList"
        //          << " name:" << appInfo->name();
        retnList.append(qVariantFromValue((QObject*)appInfo));
    }

    return retnList;
}

QString PackageManager::execPath(const QString &name)
{
    AppInfo* appInfo = m_appInfoMap.value(name,0);
    QString appExecPath ="";
    if(appInfo)
    {
        appExecPath = appInfo->path() +"/"+ appInfo->exec();
    }
    return appExecPath;
}

QString PackageManager::appType(const QString &name)
{
    AppInfo* appInfo = m_appInfoMap.value(name,0);
    QString appType ="";
    if(appInfo)
    {
        appType = appInfo->type();
    }
    return appType;
}

AppInfoMap PackageManager::appInfoMap()
{
    return m_appInfoMap;
}

void PackageManager::reload()
{
    clearAll();
    load();
    loadUninstalled();
}

bool PackageManager::install(const QString& key)
{
    // TODO:
    AppInfo* appInfo = m_uninstalledAppInfoMap.value(key,0);
    if(appInfo)
    {
        QString appsPath = qgetenv("PODO_APPS_PATH");

        if (appsPath.isEmpty())
            appsPath = PD_DEFAULT_APPS_PATH;
        QString appSetupPath = appsPath + "/" + appInfo->keys();

#ifdef Q_OS_LINUX
        QString command = QString("unzip -o \"%1\" -d \"%2\"")
                .arg(appInfo->packagePath(), appSetupPath);
        system(qPrintable(command));
#endif
        if(QDir(appSetupPath).exists())
        {
            return true;
        }
    }
    return false;
}

bool PackageManager::remove(const QString& key)
{
    AppInfo* appInfo = m_appInfoMap.value(key,0);
    QString appPath ="";
    if(appInfo)
    {
        appPath = appInfo->path();
        if(!appPath.isNull()&&!appPath.isEmpty())
        {
            bool result =  removeDir(appPath);
            if(result)
            {
                m_appInfoList.removeAll(appInfo);
                m_appInfoMap.remove(key);
                delete appInfo;

            }
            return result;
        }
    }
    return false;
}

QVariant PackageManager::appInfoVariant(const QString &key)
{
    QVariant appInfoVariant;
    if(m_appInfoMap.contains(key))
        appInfoVariant =qVariantFromValue((QObject*)m_appInfoMap.value(key));
    return appInfoVariant;
}

QVariant PackageManager::packageAppInfoVariant(const QString &key)
{
    QVariant appInfoVariant;
    if(m_uninstalledAppInfoMap.contains(key))
        appInfoVariant =qVariantFromValue((QObject*)m_uninstalledAppInfoMap.value(key));
    return appInfoVariant;
}
