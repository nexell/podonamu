TEMPLATE = lib
DESTDIR = ..
TARGET = pdpackagemanager
CONFIG += static
QT += 

MOC_DIR = ./.moc
OBJECTS_DIR = ./.obj

INCLUDEPATH += ../common

HEADERS += appinfo.h \
           packagemanager.h

SOURCES += appinfo.cpp \
           packagemanager.cpp


isEmpty($${PREFIX}) {
 PREFIX = /usr/local
}

target.path = $${PREFIX}/podo/libs/$${TARGET}

INSTALLS += target
