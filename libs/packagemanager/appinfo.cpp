#include "appinfo.h"
#include <QLocale>

AppInfo::AppInfo() : QObject()
{
}

QString AppInfo::type() const
{
    return m_type;
}

void AppInfo::setType(const QString& type)
{
    m_type = type;
}

QString AppInfo::version() const
{
    return m_version;
}

void AppInfo::setVersion(const QString& version)
{
    m_version = version;
}

QString AppInfo::name() const
{
    return m_name;
}

void AppInfo::setName(const QString& name)
{
    m_name = name;
}

QString AppInfo::genericName() const
{
    return m_genericName;
}

void AppInfo::setGenericName(const QString& genericName)
{
    m_genericName = genericName;
}

QString AppInfo::comment() const
{
    return m_comment;
}

void AppInfo::setComment(const QString& comment)
{
    m_comment = comment;
}

QString AppInfo::icon() const
{
    return m_icon;
}

void AppInfo::setIcon(const QString& icon)
{
    m_icon = icon;
}

QString AppInfo::tryExec() const
{
    return m_tryExec;
}

void AppInfo::setTryExec(const QString& tryExec)
{
    m_tryExec = tryExec;
}

QString AppInfo::exec() const
{
    return m_exec;
}

void AppInfo::setExec(const QString& exec)
{
    m_exec = exec;
}

QString AppInfo::path() const
{
    return m_path;
}

void AppInfo::setPath(const QString& path)
{
    m_path = path;
}

QString AppInfo::url() const
{
    return m_url;
}

void AppInfo::setUrl(const QString& url)
{
    m_url = url;
}

QString AppInfo::keys() const
{
    return m_keys;
}

void AppInfo::setKeys(const QString& keys)
{
    m_keys = keys;
}

bool AppInfo::installed() const
{
    return m_installed;
}

void AppInfo::setInstalled(bool installed)
{
    m_installed = installed;
}

QString AppInfo::packagePath() const
{
    return m_packagePath;
}

void AppInfo::setPackagePath(const QString &packagePath)
{
    m_packagePath = packagePath;
}
