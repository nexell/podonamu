#/bin/sh

#insmod /usr/bin/mali.ko
mkdir /podo/xdg
export XDG_RUNTIME_DIR=/podo/xdg
/podo/pdwindowcompositor -platform eglfs &
export QT_QPA_PLATFORM=wayland
export QT_LOGGING_RULES=qt.qpa.*=true
sleep 1

cd apps/pdlauncher

./pdlauncher &
