#!/bin/sh
SRC_DIR=./
BUILD_DIR=../build-podonamu
INSTALL_DIR=podo
APPS="pdlauncher pdwebengineview pdhellocpp pdhelloqml pdimageview pdsetting textures"

rm -rf $INSTALL_DIR
mkdir -p $INSTALL_DIR

cp $BUILD_DIR/core/podo $INSTALL_DIR
cp $BUILD_DIR/apps/pdwindowcompositor/pdwindowcompositor $INSTALL_DIR

mkdir -p $INSTALL_DIR/apps

for i in $APPS
do
	echo "Copying $i..."
	mkdir -p $INSTALL_DIR/apps/$i
	cp -a -r $BUILD_DIR/apps/$i/$i $INSTALL_DIR/apps/$i/
	cp -a -r $SRC_DIR/apps/$i/pkg/* $INSTALL_DIR/apps/$i/
done
cp -a -r $SRC_DIR/apps/pdhelloweb/ $INSTALL_DIR/apps/

cp -r *.sh $INSTALL_DIR/
cp kms_config $INSTALL_DIR/
echo "Done"


