#!/bin/sh
TARGET_DEVICE=s100

export TSDIR=/podo/platforms/$TARGET_DEVICE/tslib
export LD_LIBRARY_PATH=$TSDIR/lib
export TSLIB_TSDEVICE=/dev/input/event0
export TSLIB_CONFFILE=$TSDIR/etc/ts.conf
export TSLIB_PLUGINDIR=$TSDIR/lib/ts
export TSLIB_CALIBFILE=$TSDIR/pointercal 

export PODO_PATH=/podo
export PODO_APPS_PATH=$PODO_PATH/apps

export QT_DIR=/podo/platforms/$TARGET_DEVICE/Qt-Embedded-4.8.6
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QT_DIR/lib
export QWS_MOUSE_PROTO=tslib:/dev/input/event0
export QT_PLUGIN_PATH=$QT_DIR/plugins/
export QT_QWS_FONTDIR=$QT_DIR/lib/fonts/

$PODO_PATH/core/podo
