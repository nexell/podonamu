#include "core.h"
#include "global.h"

#include <QDir>
#include <QProcess>
#include <QDebug>

Core::Core(QObject* parent)
    : QObject(parent)
{
}

void Core::execute()
{
    QString appsPath = qgetenv("PODO_APPS_PATH");

    if (appsPath.isEmpty())
        appsPath = PD_DEFAULT_APPS_PATH;

    QString startApp = qgetenv("PODO_START_APP");

    if (startApp.isEmpty())
        startApp = PD_DEFAULT_START_APP;

    QString startAppPath = appsPath + "/" + startApp;

    QDir dir(startAppPath);

    if (!dir.exists(startAppPath))
    {
        qWarning() << "Core::exec not found start app path:" << startAppPath;
        return;
    }

    QProcess proc(this);

    QString execPath = startAppPath + "/" + startApp;

#ifdef Q_WS_QWS

#if QT_VERSION >= 0x050000
    execPath = execPath + " -platform linuxfb";
#else
    execPath = execPath + " -qws";
#endif

#endif

    qDebug() << "Core::exec run processor:" << execPath;

    QProcess::startDetached(execPath);

    //    proc.start(execPath);
    //    proc.waitForFinished();
}
