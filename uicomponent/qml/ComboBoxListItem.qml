import QtQuick 2.0

import QtQuick 2.6
import Qt.labs.controls 1.0

Item {
    anchors.left: parent.left
    anchors.right: parent.right
    anchors.leftMargin: parent.width /14
    property alias label: label
    property alias comboBox: comboBox
    Row {
        spacing: parent.width /10
        Text {
            anchors.leftMargin: parent.width /20
            width : parent.width *0.2
            height: 50
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            id: label
            text: qsTr("Label")
        }
        ComboBox {
            id: comboBox

          //  width : parent.width *0.7
            height: 50
        }
    }
}
