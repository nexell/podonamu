import QtQuick 2.6
import QtQuick.Layouts 1.1
import Qt.labs.controls 1.0

Item {
    id: root
    anchors.left: parent.left
    anchors.right: parent.right
    height: 50

    property alias iconSource: icon.source
    property alias title: title.text
    property alias checked: switchItem.checked

    signal clicked()

    Rectangle {
        id: backgroundRect
        anchors.fill: parent
        color: "white"
    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            backgroundRect.color = "lightgray"
        }
        onReleased: {
            backgroundRect.color = "white"
        }

        onClicked: {
            root.clicked()
        }
    }

    RowLayout {
        anchors.fill: parent

        Image {
            id: icon
            anchors.left: parent.left
            width : root.height*0.8
            height: root.height*0.8
            anchors.leftMargin: height * 0.2
            fillMode :Image.PreserveAspectFit
            sourceSize.width: width
            sourceSize.height: height
            smooth: true
            anchors.verticalCenter: parent.verticalCenter
        }


        Text {
            id: title
            anchors.left: parent.left
            anchors.leftMargin: root.height

            horizontalAlignment: Text.AlignLeft
            anchors.verticalCenter: parent.verticalCenter
            text: ""
        }

        Switch {
            id: switchItem
            anchors.right: parent.right
            anchors.rightMargin: root.height
            checked: false
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.leftMargin: root.height
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        height: 1
        color: "darkgray"
    }


}
