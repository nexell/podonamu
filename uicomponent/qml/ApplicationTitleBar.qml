import QtQuick 2.6
import Qt.labs.controls 1.0

Item {
    id                                  :   titleBar
    property alias titleText            :   titleText.text
    property alias titleImageSource     :   titleImage.source
    property alias titleColor           :   rectangle.color
    property alias backButtonVisible    :   backButton.visible
    property alias closeButtonVisible   :   closeButton.visible
    signal clickedbackButton

    anchors.top             :   parent.top
    anchors.left            :   parent.left
    anchors.right           :   parent.right
    height                  :   parent.height /10

    Rectangle {
        id                  :   rectangle
        anchors.fill        :   parent
    }
    Image {
        id                  :   titleImage
        anchors.fill        :   parent
        fillMode            :   Image.Stretch
    }
    Text {
        id                  :   titleText
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom
        anchors.left        :   parent.left
        anchors.right       :   parent.right
        text                :   "Title"
        font.pixelSize      :   parent.height * 0.8
        horizontalAlignment :   Text.AlignHCenter
        verticalAlignment   :   Text.AlignVCenter
    }
    Button {
        id                  :   closeButton
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom
        anchors.right       :   parent.right
        height              :   parent.height * 0.8
        width               :   parent.height * 0.8
        background          :   Image {
            source          :   "qrc:/PodonamuUI/Icons/navigation/ic_close_white_512dp.png"
        }
    }
    Button {
        id                  :   backButton
        anchors.top         :   parent.top
        anchors.bottom      :   parent.bottom
        anchors.left        :   parent.left
        height              :   parent.height * 0.8
        width               :   parent.height * 0.8
        Image {
            anchors.fill: parent
            source:"qrc:/PodonamuUI/Icons/hardware/ic_keyboard_return_black_48dp.png"
        }
        onClicked           :   {
            clickedbackButton()
        }
    }
}
