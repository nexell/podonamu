import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0
import QtQuick.Window 2.0
ApplicationWindow {
    id: window
    visible: true
    property bool backButtonVisible: true
    property bool closeButtonVisible: true

    signal clickedBackButton()
    signal clickedCloseButton()
    flags : Qt.platform.os==="linux" ? Qt.Window| Qt.FramelessWindowHint : Qt.Window
    //visibility : Qt.platform.os==="linux" ?  Window.FullScreen : Window.AutomaticVisibility

    header: ToolBar {
        id: headerToolBar
        RowLayout {
            anchors.fill: parent

            ToolButton {
                visible:backButtonVisible
                contentItem: Image {
                    anchors.fill: parent
                    source: "qrc:/PodonamuUI/Icons/hardware/ic_keyboard_return_white_48dp.png"
                    smooth: true
                }
                onClicked: {
                    clickedBackButton()
                }
            }

            Label {
                id: titleLabel
                anchors.right: parent.right
                anchors.left: parent.left
                text:  window.title
                font.pixelSize: headerToolBar.availableHeight * 0.7
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
                color: "white"
            }

            ToolButton {
                visible: closeButtonVisible
                contentItem: Image {
                    anchors.centerIn: parent
                    source: "qrc:/PodonamuUI/Icons/navigation/ic_close_white_512dp.png"
                    smooth: true
                }
                onClicked: {
                    closePopup.visible =true
                }
            }
        }
    }
    PDMessagePopup {
        id: closePopup
        text: qsTr("Do you want to close this appliction?")
        onClickedYes: {
            Qt.quit()
        }
        onClickedNo: {
            visible =false
        }
    }
    Component.onCompleted: {
        if(Qt.platform.os==="linux")
            showFullscreen()
    }

    function showFullscreen()
    {
        window.x = 0
        window.y = 0
        window.width = Screen.width
        window.height = Screen.height
    }
}
