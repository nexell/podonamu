import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0

Popup {
    id:popup
    width: parent.width * 0.7
    height: parent.height * 0.5
    x: (parent.width - width)/2
    y: (parent.height - height)/2
    closePolicy :  Popup.OnPressOutside
    property alias title: titleText.text
    property alias text: bodyText.text
    signal clickedYes
    signal clickedNo    

    ColumnLayout{
        spacing: 10
        anchors.fill: parent
        Text {
            id : titleText
            width: parent.width*1
            font.pixelSize : 25
            font.bold: true
            wrapMode: Text.WordWrap
            horizontalAlignment :Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
        }


        Text {
            id: bodyText

         //   anchors.fill: parent

            font.pixelSize : 20
            wrapMode: Text.WordWrap
            horizontalAlignment :Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

        RowLayout {
            id: buttonRowLayout
            spacing: 10
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            Button {
                id: yesButton
                text: qsTr("Yes")
                onClicked: clickedYes()
            }
            Button {
                id: noButton
                text: qsTr("No")
                onClicked: clickedNo()
            }
        }
    }

}
