import QtQuick 2.6
import Qt.labs.controls 1.0

Item {
    property alias label: label
    property alias textEdit: textEdit
    Row {
        anchors.fill: parent
        spacing: parent.width /10
        Text {
            anchors.leftMargin: parent.width /20
            width : parent.width *0.2
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            id: label
            text: qsTr("Label")
        }
        TextEdit {
            id: textEdit
            anchors.rightMargin: parent.width /20
            width : parent.width *0.3
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right

            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            cursorVisible: false

            Rectangle {
                anchors.fill: parent
                color: "steelblue"
                radius: 2
                opacity: 0.2
            }
        }
    }
}
