import QtQuick 2.6
import Qt.labs.controls 1.0

Popup {
    id: popup
    width: 250
    height: 200
    contentWidth: 250
    contentHeight: 200
    x: (parent.width -contentWidth)/2
    y: (parent.height -contentHeight)/2
    modal: true
    focus: true

    //closePolicy: Popup.OnEscape | Popup.OnPressOutside
    property string name: "name"
    property alias value: textInputItem.text
    signal changeValue(var value)


    Column {
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10

        Row
        {
            spacing: 10
            Text {
                width: contentWidth
                height: okButton.height
                text: name

                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
            }

            TextInput {
                id: textInputItem
                width: 130
                height: okButton.height
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                cursorVisible: false

                Rectangle {
                    anchors.fill: parent
                    color: "steelblue"
                    radius: 2
                    opacity: 0.3
                }
            }
        }

        Row {
            spacing: 10
            anchors.right: parent.right

            Button {
                id: okButton
                text: qsTr("OK")
                onClicked: changeValue(textInputItem.text)
            }
            Button {
                id: cancelButton
                text: qsTr("Cancel")
                onClicked: popup.close()
            }
        }
    }
}
