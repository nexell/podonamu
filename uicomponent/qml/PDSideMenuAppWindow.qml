import QtQuick 2.0

import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0
import QtQuick.Window 2.0

ApplicationWindow {
    id: window
    visible: true
    property bool drawerButtonVisible: true
    property bool closeButtonVisible: true
    property variant drawerModel: 0
    flags : Qt.platform.os==="linux" ? Qt.Window| Qt.FramelessWindowHint : Qt.Window

    signal clickedDrawerButton()
    signal clickedCloseButton()
    header: ToolBar {
        id: headerToolBar
        RowLayout {
            anchors.fill: parent

            ToolButton {
                visible:drawerButtonVisible
                contentItem: Image {
                    anchors.fill: parent
                    source: "qrc:/PodonamuUI/Icons/navigation/ic_menu_white_48dp.png"
                    smooth: true
                }
                onClicked: {
                    clickedDrawerButton()
                    drawer.open()
                }
            }

            Label {
                id: titleLabel
                anchors.right: parent.right
                anchors.left: parent.left
                text:  window.title
                font.pixelSize: headerToolBar.availableHeight * 0.7
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
                color: "white"
            }

            ToolButton {
                visible: closeButtonVisible
                contentItem: Image {
                    anchors.centerIn: parent
                    source: "qrc:/PodonamuUI/Icons/navigation/ic_close_white_512dp.png"
                    smooth: true
                }
                onClicked: {
                    closePopup.visible =true
                }
            }
        }
    }
    PDMessagePopup {
        id: closePopup        
        text: qsTr("Do you want to close this appliction?")

        onClickedYes: {
            Qt.quit()
        }
        onClickedNo: {
            visible =false
        }
    }


    Drawer {
        id: drawer

        Pane {
            padding: 0
            width: (Math.min(window.width, window.height) / 3 * 2)
            height: (window.height)

            ListView {
                id: listView
                currentIndex: -1
                anchors.fill: parent

                delegate: ItemDelegate {
                    width: parent.width

                    RowLayout {
                        anchors.verticalCenter: parent.verticalCenter
                        //padding: 5
                        spacing: 15

                        Image {
                            id: image
                            anchors.verticalCenter: parent.verticalCenter
                            source: model.icon
                        }

                        Label {
                            anchors.left: image.right
                            anchors.leftMargin: 5
                            anchors.verticalCenter: parent.verticalCenter
                            text: model.title
                        }


                    }

                    highlighted: ListView.isCurrentItem
                    onClicked: {
                        if (listView.currentIndex != index) {
                            listView.currentIndex = index
                            titleLabel.text = model.title
                            loader.source = model.source
                        }
                        drawer.close()
                    }
                }

                model: drawerModel

                ScrollIndicator.vertical: ScrollIndicator { }
            }
        }
        onClicked: close()
    }

    Loader {
        id: loader
        anchors.fill: parent
    }
    Component.onCompleted: {
        if(Qt.platform.os==="linux")
            showFullscreen()
    }

    function showFullscreen()
    {
        window.x = 0
        window.y = 0
        window.width = Screen.width
        window.height = Screen.height
    }
}
