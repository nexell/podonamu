import QtQuick 2.6
import Qt.labs.controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Window 2.0


ApplicationWindow {
    id:window
    visible: true
    width: 1024
    height: 600
    title: qsTr("Hello QML")
    flags : Qt.platform.os==="linux" ? Qt.Window| Qt.FramelessWindowHint : Qt.Window
    header: ToolBar {
        RowLayout {
            spacing: 20
            anchors.fill: parent
            Label {
                id: titleLabel
                anchors.right: parent.right
                anchors.rightMargin: -5
                anchors.left: parent.left
                anchors.leftMargin: -5
                text:  "Hello QML"
                font.pixelSize: 20
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }

            ToolButton {
                text: "X"
                onClicked: close()
            }
        }
    }

    Label {
        text: qsTr("Hello QML")
        anchors.centerIn: parent
    }

    Component.onCompleted: {
        if(Qt.platform.os==="linux")
            showFullscreen()
    }

    function showFullscreen()
    {
        window.x = 0
        window.y = 0
        window.width = Screen.width
        window.height = Screen.height
    }
}
