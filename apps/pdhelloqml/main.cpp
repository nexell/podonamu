#include <QApplication>
#include <QQmlApplicationEngine>

int main(int argc, char *argv[])
{
    // High-DPI Support
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}
