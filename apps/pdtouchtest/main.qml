import QtQuick 2.6
import Qt.labs.controls 1.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0

ApplicationWindow {
    visible: true
    width: 1024
    height: 600
    title: qsTr("Touch Test")
    id: mainWindow
    flags : Qt.platform.os==="linux" ? Qt.Window| Qt.FramelessWindowHint : Qt.Window
    color: "white"
    Rectangle {
        id: touchRect
        width: 10
        height: 10
        radius: 5
        x:touchpoint.x - touchRect.width/2
        y:touchpoint.y - touchRect.height/2
        color: "red"
    }

    MultiPointTouchArea {
        anchors.fill: parent
       mouseEnabled: true
       touchPoints: [
            TouchPoint{id:touchpoint}
       ]
        onPressed: {
            for(var i=0; i<touchPoints.count; i++) {
                var touchPoint = touchPoints[i]
                console.log("touch point x:",touchpoint.x ,"touch point y:",touchpoint.y)
                touchRect.x = touchpoint.x - touchRect.width/2
                touchRect.y = touchpoint.y - touchRect.height/2
            }
        }
    }

    Component.onCompleted: {
        if(Qt.platform.os==="linux")
            showFullscreen()
    }

    function showFullscreen()
    {
        mainWindow.x = 0
        mainWindow.y = 0
        mainWindow.width = Screen.width
        mainWindow.height = Screen.height
        console.log("screen width:", Screen.width, " screen height:", Screen.height)
    }
}
