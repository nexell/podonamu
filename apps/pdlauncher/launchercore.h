#ifndef LAUNCHERCORE_H
#define LAUNCHERCORE_H

#include <QObject>
#include <QVariant>

class LauncherCore : public QObject
{
    Q_OBJECT

public:
    explicit LauncherCore(QObject *parent = 0);
    Q_INVOKABLE QVariant loadHomeIcon();
    Q_INVOKABLE bool saveHomeSettring(QVariant );

signals:

public slots:
};

#endif // LAUNCHERCORE_H
