#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QtQml>

#include "packagemanager/packagemanager.h"
#include "processmanager/processmanager.h"
#include "networkmanager/networkmanager.h"
#include "applicationmanager/applicationmanager.h"
#include "launchercore.h"

int main(int argc, char *argv[])
{
    // High-DPI Support
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    qmlRegisterType<PackageManager>("podo.packagemanager", 1, 0, "PackageManager");
    qmlRegisterType<AppInfo>("podo.packagemanager", 1, 0, "AppInfo");
    qmlRegisterType<ProcessManager>("podo.processmanager", 1, 0, "ProcessManager");
    qmlRegisterType<NetworkManager>("podo.networkmanager", 1, 0, "NetworkManager");
    qmlRegisterType<ApplicationManager>("podo.applicationmanager", 1, 0, "ApplicationManager");
    qmlRegisterType<ApplicationManager>("podo.launchercore", 1, 0, "LauncherCore");
    PackageManager* packageManager = new PackageManager;
    ProcessManager* processManager = new ProcessManager;
    NetworkManager* networkManager = new NetworkManager;
    ApplicationManager* applicationManager = new ApplicationManager;
    LauncherCore* core = new LauncherCore;
    processManager->setPackageManager(packageManager);
    processManager->setApplicationManager(applicationManager);

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("packageManager", packageManager);
    engine.rootContext()->setContextProperty("processManager", processManager);
    engine.rootContext()->setContextProperty("networkManager", networkManager);
    engine.rootContext()->setContextProperty("applicationManager", applicationManager);
    engine.rootContext()->setContextProperty("core", core);
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    QObject* mainObject =engine.rootObjects().value(0);

    if(mainObject)
    {
        QObject::connect(mainObject, SIGNAL(launchProgram(QString)),
                         processManager, SLOT(executeApplication(QString)));
    }

    return app.exec();
}
