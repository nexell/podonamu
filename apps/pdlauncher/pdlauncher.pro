TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

INCLUDEPATH += ../../libs
LIBS += -L../../libs -lpdpackagemanager \
    -lpdprocessmanager \
    -lpdapplicationmanager \
    -lpdnetworkmanager

SOURCES += main.cpp \
    launchercore.cpp

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model

lupdate_only{
    SOURCES +=  $$files(../../uicomponent/*.qml) \
                $$files(./qml/*.qml)

}
# Default rules for deployment.
include(deployment.pri)
include(../../uicomponent/uicomponent.pri)
DISTFILES +=

HEADERS += \
    launchercore.h
