import QtQuick 2.6
import Qt.labs.controls 1.0
import podo.packagemanager 1.0
import podo.processmanager 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

Item {
    id: pane
    property int maxColumnsInPage:0
    property int maxRowsInPage:0
    property int totalPage:3
    property int currentPage:1
    property int iconCellWidth: 100
    property int iconCellHeight: 100
    signal launchProgram(string name)

    Connections {
        target: processManager
        onFinishedApplication: {
            console.info("Home.qml onFinishedApplication name : ", name)
            if(name ==="pdsetting")
            {
                packageManager.reload()
                updateAppList()
            }
        }
    }

    ListModel {
        id: appListModel
    }

    SwipeView {
        id: view
        currentIndex: currentPage
        anchors.fill: parent
        anchors.leftMargin: width%iconCellWidth /2

        Repeater {
            id: totalPageRepeater
            model: totalPage

            GridView {
                id: pageGridView
                cellWidth: iconCellWidth
                cellHeight: iconCellHeight
                interactive: false
                clip: true
                model:maxColumnsInPage *maxRowsInPage
                delegate: PodonamuUI.DropIcon {
                    width: iconCellWidth
                    height: iconCellHeight
                    dragKey: "icon"
                    Component.onCompleted: {

                    }
                }
            }
        }
    }

    PageIndicator {
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }

    function updateAppList() {

        appListModel.clear()
        var appInfoList = packageManager.appInfoVariantList()
        for(var idx =0 ; idx < appInfoList.length; idx++) {
            var i = idx % appInfoList.length
            var pageIdx = Math.floor(idx/(maxColumnsInPage*maxRowsInPage))
            console.log("Home.qml updateAppList() appInfo",
                        " name:", appInfoList[i].name,
                        " path:", appInfoList[i].path,
                        " icon:", appInfoList[i].icon)
            appListModel.append(
                        { "name": appInfoList[i].name,
                            "path": appInfoList[i].path,
                            "icon": appInfoList[i].icon,
                            "keys": appInfoList[i].keys
                        })
        }
        console.log("Home.qml updateAppList()",
                    " appInfoList.length:", appInfoList.length,
                    " totalPage:", totalPage,
                    " currentPage:", currentPage)

        maxColumnsInPage = Math.floor(pane.width / iconCellWidth)
        maxRowsInPage = Math.floor(pane.height / iconCellHeight)
        dragIconList.iconListUdate()
    }

    Row {
        id: dragIconList
        anchors.left: parent.left; anchors.bottom: parent.top;

        Repeater {
            id:iconRepeater
            model: appListModel
            delegate: PodonamuUI.DragIcon {
                dragKey:  "icon" ; width:iconCellWidth; height: iconCellHeight

                Connections {
                    target: mouseArea
                    onClicked: {
                        launchProgram(keys)
                    }
                }
            }
        }

        function iconListUdate()
        {
            for(var idx =0 ; idx < iconRepeater.count; idx++) {
                var item = totalPageRepeater.itemAt(1).contentItem.children[idx]
                iconRepeater.itemAt(idx).mouseArea.parent = item
            }
        }
    }


    Component.onCompleted: {
        console.log("Home.qml Component.onCompleted")
        timer.start()
    }

    Timer {
        id: timer
        interval: 100; running: false; repeat: false
        onTriggered: updateAppList()
    }
}
