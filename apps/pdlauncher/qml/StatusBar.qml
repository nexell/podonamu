import QtQuick 2.6
import Qt.labs.controls 1.0

Item {
    id: statusBar
    width: parent.width
    height: 20

    Label {
        id: timeLabel
        anchors.horizontalCenter: parent.horizontalCenter
        text: ""
    }

    Timer {
        id: statusTimer
        interval: 1000
        running: true
        repeat: true

        onTriggered: {
            updateTime()
        }
    }

    function updateTime() {
        var date = new Date()
        var timeString = date.getHours() + " : " + date.getMinutes()
        timeLabel.text = timeString
    }

    Component.onCompleted: {
        statusTimer.start()
        updateTime()
    }
}
