import QtQuick 2.6
import Qt.labs.controls 1.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0

import "qrc:/PodonamuUI/qml" as PodonamuUI

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 1024
    height: 600
    title: qsTr("Home")
    flags : Qt.platform.os==="linux" ? Qt.Window| Qt.FramelessWindowHint : Qt.Window
    signal launchProgram(string name)
    Image {
        anchors.fill: parent
        source: "qrc:/images/launcher_bg.png"
    }
    Image {
        anchors.bottom: parent.bottom
        anchors.right:  parent.right
        width: sourceSize.width /2
        height: sourceSize.height /2
        smooth: true
        source: "qrc:/PodonamuUI/images/podo-logo.png"
    }

    StatusBar {
        id: statusBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }    

    Home {
        id: home

        anchors.top: statusBar.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        onLaunchProgram: {
            console.log("main.qml onLaunchProgram() name:", name)

            mainWindow.launchProgram(name)

            // TODO: 프로그램을 실행 시켜야 한다.
            // 프로그램 실행은 process manager를 통해서 실행해야 한다.
            // ...
        }
    }
    Component.onCompleted: {
        if(Qt.platform.os==="linux")
            showFullscreen()
    }

    function showFullscreen()
    {
        mainWindow.x = 0
        mainWindow.y = 0
        mainWindow.width = Screen.width
        mainWindow.height = Screen.height
        console.log("screen width:", Screen.width, " screen height:", Screen.height)
    }
}
