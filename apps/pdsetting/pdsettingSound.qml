import QtQuick 2.6
import QtQuick.Layouts 1.1
import Qt.labs.controls 1.0
import podo.networkmanager 1.0
import podo.audiomanager 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

Flickable {
    id: flickable
    property int lineHeight: height*
                             (width > height
                              ? 0.15: 0.10)
    ColumnLayout {
        id: topColumn
        width: parent.width
        spacing: 0


        PodonamuUI.SliderListItem {
            id: brightSlider
            title: qsTr("Sound Volume")
            from: 0
            to: 100
            stepSize: 1
            enabled: audioManager.isDefaultOutputAudioDevice()
            value: audioManager.getAudioVolume()
            height: lineHeight
            onValueChanged: {
                audioManager.setAudioVolume(value)
            }
        }
    }

    ScrollIndicator.vertical: ScrollIndicator { }

}
