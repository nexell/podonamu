TEMPLATE = app

QT += qml quick core multimedia

CONFIG += c++11

INCLUDEPATH += ../../libs
LIBS += -L../../libs -lpdnetworkmanager \
    -lpdaudiomanager \
    -lpdpackagemanager

linux-* {
    LIBS +=    -lasound
}
message($$QMAKESPEC)

SOURCES += main.cpp

RESOURCES += qml.qrc \
    images.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
include(../../uicomponent/uicomponent.pri)
DISTFILES +=
