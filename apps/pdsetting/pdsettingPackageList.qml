import QtQuick 2.6
import QtQuick.Layouts 1.1
import Qt.labs.controls 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

Flickable {
    id : flickable
    //contentHeight: parent.height
    property int lineHeight: height*
                             (width > height
                              ? 0.15: 0.10)
    ListModel {
        id:appListModel
    }
    ListView {
        id : appListView
        model: appListModel
        anchors.fill: parent
        delegate: PodonamuUI.ArrowImageListItem {
            width: parent.width- 20
            height: lineHeight
            iconSource: {
                if (path.length === 0 || icon.length === 0) {
                    return "qrc:/PodonamuUI/images/noicon.png"
                }
                var iconPath = "file:/" + path + "/" + icon
                return iconPath
            }
            title: name
            onClicked: {
                var component = Qt.createComponent("PackageDetailView.qml")
                if(component !==null)
                {
                    var detailView = component.createObject(flickable)
                    detailView.key = keys
                    detailView.appName = name
                    detailView.iconPath = iconSource
                    detailView.deletedApp.connect(deletedApp)
                    detailView.installApp.connect(deletedApp)
                }
            }
        }
    }

    function deletedApp(key) {
        packageManager.reload()
        update()
    }

    Component.onCompleted: update()


    function update() {
        appListModel.clear()
        var appInfoList = packageManager.appInfoVariantList()
        for(var idx =0 ; idx < appInfoList.length; idx++) {
            console.log("pdsettingPackageList.qml updateAppList() appInfo",
                        " name:", appInfoList[idx].name,
                        " path:", appInfoList[idx].path,
                        " icon:", appInfoList[idx].icon,
                        " isInstalled:", appInfoList[idx].installed)
            appListModel.append(appInfoList[idx])
        }

        var packagedAppInfoList = packageManager.uninstalledAppInfoVariantList()
        for(var idx =0 ; idx < packagedAppInfoList.length; idx++) {
            console.log("pdsettingPackageList.qml updateAppList() appInfo",
                        " name:", packagedAppInfoList[idx].name,
                        " path:", packagedAppInfoList[idx].path,
                        " icon:", packagedAppInfoList[idx].icon,
                        " isInstalled:", packagedAppInfoList[idx].installed)
            if(packagedAppInfoList[idx].installed)
                continue

            appListModel.append(packagedAppInfoList[idx])

        }
    }
}
