#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QtQml>
#include <QRect>
#include <QDebug>
#include <QScreen>
#include "networkmanager/networkmanager.h"
#include "audiomanager/audiomanager.h"
#include "packagemanager/packagemanager.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setApplicationName("pdsetting");
    QGuiApplication::setOrganizationName("podo");
    //QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qreal refDpi = 96.;
    qreal refHeight = 600.;
    qreal refWidth = 1024.;
    QRect rect = qApp->primaryScreen()->geometry();
    qreal height = qMax(rect.width(), rect.height());
    qreal width = qMin(rect.width(), rect.height());
    qreal dpi = qApp->primaryScreen()->logicalDotsPerInch();
    qreal podoScreenRatio = qMin(height/refHeight, width/refWidth);
    qreal podoScreenRatioFont = qMin(height*refDpi/(dpi*refHeight),
                                      width*refDpi/(dpi*refWidth));

    qDebug() << "screen width" << width << "height:" << height << "dpi:" << dpi;

#ifdef Q_OS_ANDROID
    osType = 1;
#else
    podoScreenRatio = 1;
    podoScreenRatioFont = 1;
#endif
    qDebug() << "podoScreenRatio:" << podoScreenRatio;

    QString checkOS = "";
#ifdef Q_OS_WIN
    checkOS = "windows";
#endif

    QSettings settings;
    qputenv("QT_LABS_CONTROLS_STYLE", settings.value("style").toByteArray());

    qmlRegisterType<NetworkManager>("podo.networkmanager", 1, 0, "NetworkManager");
    qmlRegisterType<AudioManager>("podo.audiomanager", 1, 0, "audioManager");
    qmlRegisterType<PackageManager>("podo.packagemanager", 1, 0, "packageManager");
    NetworkManager* networkManager = new NetworkManager;
    AudioManager* audioManager = new AudioManager;
    PackageManager* packageManager=new PackageManager;
    networkManager->loadNetworkInfo();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("networkManager", networkManager);
    engine.rootContext()->setContextProperty("audioManager", audioManager);
    engine.rootContext()->setContextProperty("packageManager", packageManager);
    engine.rootContext()->setContextProperty("podoScreenRatio", podoScreenRatio);
    engine.rootContext()->setContextProperty("checkOS", checkOS);

    engine.load(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
