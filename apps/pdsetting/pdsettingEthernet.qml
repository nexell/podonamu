import QtQuick 2.6
import QtQuick.Layouts 1.1
import Qt.labs.controls 1.0
import podo.networkmanager 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

Flickable {
    id: flickable
    property string ipText: ""
    property string netmaskText: ""
    property string gatewayText: ""
    property int lineHeight: height*
                             (width > height
                              ? 0.15: 0.10)

    ColumnLayout {
        id: topColumn
        width: parent.width
        spacing: 0

        PodonamuUI.ArrowImageListItem {
            id: interfaceNameItem
            title: qsTr("Interface")
            value: ""
            height: lineHeight

            onClicked: {
                console.log("pdsettingEthernet.qml interface clicked")
            }
        }

        PodonamuUI.SwitchListItem {
            id: dhcpSwitchItem
            title: qsTr("DHCP")
            checked: false
            height: lineHeight
        }

        ColumnLayout {
            id: middleColumn
            width: parent.width

            enabled: dhcpSwitchItem.checked

            PodonamuUI.ArrowImageListItem {
                id: ipAddressItem
                title: qsTr("Set Ip Address")
                value: ""
                height: lineHeight
                onClicked: {

                }
            }

            PodonamuUI.ArrowImageListItem {
                id :subnetItem
                title: qsTr("Subnet Mask")
                value: ""
                height: lineHeight
                onClicked: {
                }
            }

            PodonamuUI.ArrowImageListItem {
                id: gatewayItem
                title: qsTr("Set Gateway")
                height: lineHeight
                onClicked: {
                }
            }
            Row {
                spacing: 10
                visible: dhcpSwitchItem.checked
                         &&(ipAddressItem.value !== ipText
                            || subnetItem.value !== netmaskText
                            || gatewayItem.value !== gatewayText)
                anchors.horizontalCenter: parent.horizontalCenter
                Button {
                    id : confirmButton
                    text: qsTr("Confirm")
                    onClicked: {
                        networkManager.setDhcpNetwork(dhcpSwitch.value)
                        if (!dhcpSwitch.value) {
                            var ipAddress = ipTextInput.value
                            var submask = subnetTextInput.value
                            var gateway = gatewayTextInput.value
                            networkManager.setIpInformationNetwork(ipAddress, submask, gateway)
                        }
                    }
                }
                Button {
                    id : cancelButton
                    text: qsTr("Cancel")
                    onClicked: {
                        ipSwitch.checked = false
                    }
                }
            }
        }
    }

    ScrollIndicator.vertical: ScrollIndicator { }

    Component.onCompleted: {
        var interfaceList = networkManager.interfaceList
        if(interfaceList.length>0)
        {
            interfaceNameItem.value = interfaceList[0]
            var networkInfo = networkManager.changedNetworkInterface(interfaceList[0])
            dhcpSwitchItem.checked = networkInfo["isDhcp"]
            ipAddressItem.value = networkInfo["ip"]
            subnetItem.value = networkInfo["netmask"]
            gatewayItem.value = networkInfo["defaultGateway"]
            ipText = networkInfo["ip"]
            netmaskText = networkInfo["netmask"]
            gatewayText = networkInfo["defaultGateway"]
        }

    }
}
