import QtQuick 2.6
import Qt.labs.controls 1.0
import "qrc:/js/common.js" as CommonJS

Pane {
    anchors.top: parent.top
    anchors.topMargin: 30

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10

        Row {
            spacing: 19

            Label {
                width: 250
                height: 65
                horizontalAlignment: Label.AlignRight
                verticalAlignment: Label.AlignVCenter

                text: qsTr("IP Address")
            }

            TextInput {
                id: ipAddressTextInput
                width: 250
                height: 60
                font.pixelSize: 30
                font.bold: true
                text: ""
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                cursorVisible: false

                Rectangle {
                    anchors.fill: parent
                    color: "steelblue"
                    radius: 2
                    opacity: 0.3

                }
            }
        }

        Row {
            spacing: 19

            Label {
                width: 250
                height: 65
                horizontalAlignment: Label.AlignRight
                verticalAlignment: Label.AlignVCenter

                text: qsTr("Subnet Mask")
            }

            TextInput {
                id: subnetMaskTextInput
                width: 250
                height: 60
                font.pixelSize: 30
                font.bold: true
                text: ""
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                cursorVisible: false

                Rectangle {
                    anchors.fill: parent
                    color: "steelblue"
                    radius: 2
                    opacity: 0.3

                }
            }
        }

        Row {
            spacing: 19

            Text {
                width: 250
                height: 65
                font.pixelSize: 35
                font.bold: true
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter

                text: qsTr("Gateway")
            }

            TextInput {
                id: gatewayTextInput
                width: 250
                height: 60
                font.pixelSize: 30
                font.bold: true
                text: ""
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                cursorVisible: false

                Rectangle {
                    anchors.fill: parent
                    color: "steelblue"
                    radius: 2
                    opacity: 0.3

                }
            }
        }
    }

    Row {
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.top: parent.top
        anchors.topMargin: 20

        spacing: 5
        Rectangle {
            id: backRect
            width: 80
            height: 40
            color: "blue"
            radius: 20

            Text {
                anchors.fill: parent
                text: qsTr("BACK")
                color: "white"
                font.pixelSize: 20
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    ipSetting.destroy()
                }
            }
        }

        Rectangle {
            id: okRect
            width: 80
            height: 40
            color: "blue"
            radius: 20

            Text {
                anchors.fill: parent
                text: qsTr("OK")
                color: "red"
                font.pixelSize: 20
                font.bold: true
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }

            MouseArea {
                anchors.fill: parent

                onClicked: {
                    var ipAddress = ipAddressTextInput.text
                    var subnetMask = subnetMaskTextInput.text
                    var gateway = gatewayTextInput.text

                    //TODO core.cpp ip설정 연결하기

                    ipSetting.destroy()
                }
            }
        }
    }
}

