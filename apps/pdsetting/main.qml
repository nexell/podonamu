import QtQuick 2.6
import QtQuick.Layouts 1.3
import Qt.labs.controls 1.0
import podo.networkmanager 1.0
import podo.packagemanager 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

PodonamuUI.PDSideMenuAppWindow
{
    id: window
    visible: true
    width: 1024 * podoScreenRatio
    height: 600 * podoScreenRatio
    title: qsTr("Setting")

    drawerModel:ListModel {
                        ListElement { title: qsTr("Ethernet");      icon: "qrc:images/ethernet.png";    source: "qrc:/pdsettingEthernet.qml" }
                        ListElement { title: qsTr("Brightness");    icon: "qrc:images/brightness.png";  source: "qrc:/pdsettingBrightness.qml" }
                        ListElement { title: qsTr("Sound");         icon: "qrc:images/sound.png";       source: "qrc:/pdsettingSound.qml" }
                        ListElement { title: qsTr("Package");       icon: "qrc:images/sound.png";       source: "qrc:/pdsettingPackageList.qml" }
                    }
    Loader {
        id: loader
        anchors.fill: parent
    }


}
