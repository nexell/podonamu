import QtQuick 2.6
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import Qt.labs.controls 1.0
import podo.packagemanager 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

Page {
    id : packageDetailView
    anchors.fill : parent

    property string key : ""
    property string appName : ""
    property string iconPath : ""
    signal deletedApp(var key)
    signal installApp(var key)
    PodonamuUI.ApplicationTitleBar {
        id : titleBar
        backButtonVisible : true
        closeButtonVisible : false
        titleText : ""

        onClickedbackButton : {
            packageDetailView.destroy()
        }

    }

    Flickable {
        id : detailViewFlickable
        anchors.top : titleBar.bottom
        anchors.bottom : parent.bottom
        anchors.left : parent.left
        anchors.right : parent.right

        ColumnLayout {
            anchors.horizontalCenter : parent.horizontalCenter

            RowLayout {
                spacing : packageDetailView.width /40
                Image {
                    sourceSize.width : packageDetailView.width/ 5
                    sourceSize.height : width
                    id : iconImage
                    source : iconPath
                }

                Text {
                    id : nameText
                    text : appName
                }
            }

            RowLayout {
                anchors.left : parent.left
                anchors.right : parent.right
                Layout.fillWidth : true
                spacing : packageDetailView.width /10
                Button {
                    id : deleteButton
                    text : qsTr("Delete")
                    onClicked : {
                        deletePopup.visible = true
                    }
                }
                Button {
                    id : installButton
                    text : qsTr("Install")
                    onClicked : {
                        installPopup.visible = true
                    }
                }
            }
        }
    }


    PodonamuUI.PDMessagePopup {
        id : deletePopup
        title : qsTr("Delete Application")
        text : qsTr("Are you sure you want to delete this application?")
        onClickedYes : {
            var result = packageManager.remove(key)
            if(result)
            {
                deletedApp(key)
                packageDetailView.destroy()
            }
        }
        onClickedNo : {
            visible = false
        }
    }

    PodonamuUI.PDMessagePopup {
        id : installPopup
        title : qsTr("Install Application")
        text : qsTr("Are you sure you want to install this application?")
        onClickedYes : {
            var result = packageManager.install(key)
            console.log(result)
            if(result)
            {
                installApp(key)
                packageDetailView.destroy()
            }
        }
        onClickedNo : {
            visible = false
        }
    }

    onKeyChanged : {
        var appInfo = packageManager.appInfoVariant(key)
        var packageInfo = packageManager.packageAppInfoVariant(key)
        console.log(packageInfo)
        if(appInfo ===undefined)
        {
            deleteButton.enabled =false
        }
        if(packageInfo===undefined)
        {
            installButton.enabled =false
        }

    }
}
