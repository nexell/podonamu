import QtQuick 2.6
import Qt.labs.controls 1.0
import QtQuick.Layouts 1.1
import "qrc:/PodonamuUI/qml" as PodonamuUI

Flickable {
    id: flickable
    property int lineHeight: height*
                             (width > height
                              ? 0.15: 0.10)
    ColumnLayout {
        id: topColumn
        width: parent.width
        spacing: 0

        PodonamuUI.SwitchListItem {
            id: autoSwitch
            title: qsTr("Auto Brightness")
            checked: false
            height: lineHeight
        }

        PodonamuUI.SliderListItem {
            id: brightSlider
            title: qsTr("Brightness Level")
            from: 0
            to: 100
            stepSize: 1
            value: 100
            height: lineHeight
            onValueChanged: {
                console.log("pdsettingBrightness.qml dhcpSwitch onValueChanged  value: ", value)
            }
        }
    }

    ScrollIndicator.vertical: ScrollIndicator { }
}
