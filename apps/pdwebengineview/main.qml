import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0
import Qt.labs.folderlistmodel 2.1

//import QtWebView 1.0
import "qrc:/PodonamuUI/qml" as PodonamuUI

PodonamuUI.PDAppWindow {
    id: mainPage
    visible: true
    width: 1024
    height: 600
    title: qsTr("Web Engine View")
    backButtonVisible: false
/*
    WebView {
        anchors.fill: parent
        url: webUrl
    }
  */
    Component.onCompleted: {
        var webView= Qt.createQmlObject("import "
                                        +definedWeb
                                        +" "
                                        + webModuleVersion
                                        +" ; WebView { anchors.fill:parent; url:webUrl;}",
                                        mainPage.contentItem,
                                        "")
    }
}
