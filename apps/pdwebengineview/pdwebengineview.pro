QT += qml quick widgets gui

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

lupdate_only{
    SOURCES +=  $$files(../../uicomponent/*.qml) \
                $$files(./*.qml)

}
linux-g* {
QT +=    webview
DEFINES += USE_WEBVIEW
}
linux-oe-g* || arm-poky-linux-gnueabi-g* {
QT +=    webkit
DEFINES += USE_WEBKIT
}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
include(../../uicomponent/uicomponent.pri)

HEADERS += ../../core/global.h

DISTFILES +=
