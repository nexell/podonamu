#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QDebug>
#include <QQmlContext>
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    qDebug ()<<"pdwebengineview arguments :" << app.arguments();
    QString webUrl = "about:blank";
    if(app.arguments().length()>=3 && app.arguments().at(1) == "-url")
    {
        webUrl = app.arguments().at(2);
    }else{
        webUrl = "http://google.com";
    }

    QQmlApplicationEngine engine;
#if defined(USE_WEBVIEW)
    engine.rootContext()->setContextProperty("definedWeb", "QtWebView");
    engine.rootContext()->setContextProperty("webModuleVersion", "1.0");
#elif defined(USE_WEBKIT)
    engine.rootContext()->setContextProperty("definedWeb", "QtWebKit");
    engine.rootContext()->setContextProperty("webModuleVersion", "3.0");
#endif
    engine.rootContext()->setContextProperty("webUrl", webUrl);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));



    return app.exec();
}
