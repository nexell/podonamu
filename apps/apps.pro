TEMPLATE = subdirs
SUBDIRS = pdhelloqml pdlauncher \
    pdsetting \
    pdhellocpp \
    pdimageview \
    pdtouchtest \
	textures


!win32-* {
SUBDIRS += pdwebengineview
}


linux-oe-g* || arm-poky-linux-gnueabi-g* {
SUBDIRS += pdwindowcompositor
}