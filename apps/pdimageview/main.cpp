

#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDebug>
#include <QtQml>
int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    QString defaultPath;
#ifdef Q_OS_LINUX
    defaultPath =    "file:///podo/apps";
        #else
#if QT_VERSION < 0x050000
    defaultPath =  "file:///"+  QDesktopServices::storageLocation(QDesktopServices::DocumentsLocation) + "";
        #else
    defaultPath =   "file:///"+ QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation).at(0) + "";
        #endif // QT_VERSION < 0x050000
        #endif // Q_OS_LINUX
    qDebug() << "Main default folder path :" << defaultPath;

            engine.rootContext()->setContextProperty("defaultPath", defaultPath);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));


    return app.exec();
}
