import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0

Page {
    id:imageViewer
    anchors.fill: parent
    property string imageUrl: ""
    property string imageName: ""

    Image {
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: parent.height /20
        id: image
        source: imageUrl
        fillMode: Image.PreserveAspectFit
    }
}
