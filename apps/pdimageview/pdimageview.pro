QT += qml quick widgets

CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc \
    images.qrc

lupdate_only{
    SOURCES +=  $$files(../../uicomponent/*.qml) \
                $$files(./*.qml)

}

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)
include(../../uicomponent/uicomponent.pri)

HEADERS +=

DISTFILES +=
