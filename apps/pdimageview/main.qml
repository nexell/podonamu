import QtQuick 2.6
import QtQuick.Layouts 1.0
import Qt.labs.controls 1.0
import Qt.labs.folderlistmodel 2.1
import "qrc:/PodonamuUI/qml" as PodonamuUI

PodonamuUI.PDAppWindow {
    id: mainPage
    visible: true
    width: 1024
    height: 600
    title: qsTr("Image View") + (imageDetailView!==undefined
                                 &&imageDetailView!==null
                                 ?" - "+imageDetailView.imageName:"")
    property string folderPath : defaultPath
    property variant imageDetailView: null
    backButtonVisible: imageDetailView!=null
    FolderListModel {
        id : folderModel
        showDirs: true
        showFiles: true
        showDirsFirst: true
        showDotAndDotDot: true
        nameFilters: ["*.jpg", "*.png"]
        folder: folderPath

        onFolderChanged: {
            console.log("onFolderChanged root.folderPathName:", folderPath);
            console.log("\n               folderModel.folder:", folderModel.folder,
                        "\n         folderModel.parentFolder:", folderModel.parentFolder,
                        "\n           folderModel.rootFolder:", folderModel.rootFolder);
        }
    }
    Item {
        id: bodyItem
        anchors.fill:parent

        GridView {
            id : imageList
            model: folderModel
            clip: true
            anchors.fill: parent

            delegate: Rectangle {
                height: imageList.cellWidth
                width: imageList.cellHeight
                color: "transparent"
                Column {
                    width: parent.width-6
                    height: parent.height-6
                    anchors.centerIn:parent
                    Image {
                        id: icon
                        width: parent.width
                        height: parent.height- text.height
                        fillMode: Image.PreserveAspectFit
                        source: {
                            if(index <=0 || folderModel.isFolder(index))
                            {
                                return "qrc:/images/ic_folder_open_black_48dp.png"
                            }
                            else
                            {
                                return folderModel.get(index,"fileURL")
                            }
                        }
                    }
                    Text {
                        id : text
                        width: icon.width
                        fontSizeMode: Text.VerticalFit
                        wrapMode: Text.WordWrap
                        maximumLineCount: 1
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        text: {
                            var fileName =folderModel.get(index,"fileName")
                            if(fileName!==undefined)
                            {
                                return fileName
                            }
                            return ""
                        }
                    }
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.log("ImageView main.qml index: ", index)
                        if(folderModel.isFolder(index))
                        {
                            folderPath = folderModel.get(index, "fileURL")
                        }
                        else
                        {
                            var component = Qt.createComponent("imageviewer.qml")
                            if(component !==null)
                            {
                                imageDetailView = component.createObject(bodyItem)
                                imageDetailView.imageName = folderModel.get(index,"fileName")
                                imageDetailView.imageUrl = folderModel.get(index, "fileURL")
                            }
                        }
                    }
                }
            }
        }
    }
    onClickedBackButton: {
        if(imageDetailView!==null)
        {
            imageDetailView.destroy()
            imageDetailView = null
        }
    }
}
