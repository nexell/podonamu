QT += gui gui-private core-private compositor

HEADERS += \
    compositorwindow.h \
    qwindowcompositor.h \
    textureblitter.h

SOURCES += main.cpp \
    compositorwindow.cpp \
    qwindowcompositor.cpp \
    textureblitter.cpp
	
RESOURCES += pdwindowcompositor.qrc
